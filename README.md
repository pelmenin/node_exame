[Диаграмма](https://www.plantuml.com/plantuml/dumla/ZP9BRzKm48Nt-HM3DtaeBG-fcwiewgC1xe88XRHYWOJSP2urE7OOsseZTFyxTgAMMu94LvcS-UoPSv9JOpbPDyZeHOyxeH1QVYCnKNp0l4VJfT19RWm40RW1LYahxBI1B0jIbi4N1PSU34eua6KCYWAIDjVKtOe-XR9yl7maCN2Qllx0ARbFeN3bVV7I_UdlDA7ufXxxdPQQDgGdBi-bmq3FKXb5ovxE8EN0hlm3UhHSICDI-1K1m9DX0TXUfhCUwjdE1gxFceltPqtovkJ4dt9A_7IOWz8Mb9DoF-CVlVqxJVqAVugzzLMzkdM5ZRTQOUs66oHFdc_VRUlsRwOQl8zdRjTekHxfq-HEMeX3FF4-sZ_4HJXgiZwmQw4bjq8hQ6Rfdw4jhOVO3h_DOPmNd1O__t4UVX8IOnZgoRRwlFBzoNBhZ6zMzTM795Pu5-SGTrhj10tOXnV2xzXPNn-u3E1YCjW5zf

[PLANTUML-SCHEMA](https://www.plantuml.com/plantuml/dumla/ZLDBpzem4BpdLsnzoKE1FYGkA8g0GX-7eWf1JvKganYmS6pgXqfKyT_h3GqqKhyefxLsPsV6pcHcBDNMLI9uLR83bmosT2zO96d5qfAP8eP2K6C06q0Da9MqtDPJI19i9Gbybx3qGEpWGf8Go3A8jgdInOcNCUJvW_oYUKLr_UFCwkWQG-RoQ_RQFUS7fHa_oiUyK4BfgLOr5GlX6AARLXu4Tv_KsPFIAP1vS_1EBEN2a1X-1m3G4Rn3FY_ZPlB12R76IqtXstpp_jDy4xsRJFoUL1Qa7zw2MwjoP58pB_8HQwz6WsWbXjBlbJfxzeKllUG8QBcVBzMzM6VIjRFbLlJvQJ3rPP1bPnY--iwNNii8QFNnBF_t94xoduwbqBNorSVkbz9bZvs1tn5Xgyfx7AYyKGBfLklTboYaPSLbc4A8LmxzZeygTSB-RVoR4gtS1LE2jJybY1YU4XG2yfR0Q0JapNYS46Yq_DQU2IMF1gmAxhz22tX2CLr1a-0NPZCcIrU9Fm00)

@startuml
!define Table(name,desc) class name as "Entity: **name** \n Desc: **desc**" << (T,orchid) >>
!define primary_key(x) <u>x</u>
!define foreign_key(x) <color:royalBlue>x</color>

Table(author, "Author Details") {
    primary_key(AuthorID)
    FullName : VARCHAR(255) {not null}
}

Table(genre, "Genre Details") {
    primary_key(GenreID)
    Name : VARCHAR(255) {not null}
}

Table(book, "Book Details") {
    primary_key(BookID)
    foreign_key(AuthorID)
    foreign_key(GenreID)
    Title : VARCHAR(255) {not null}
    Type : VARCHAR(255) {not null}
}

Table(user, "User Details") {
    primary_key(UserID)
    Username : VARCHAR(255) {unique, not null}
    Password : VARCHAR(255) {not null}
    FullName : VARCHAR(255) {not null}
    Email : VARCHAR(255) {unique, not null}
    Role : ENUM('admin', 'user') {default 'user'}
}

Table(group, "Group Details") {
    primary_key(GroupID)
    Name : VARCHAR(255) {not null}
}

user "1" -- "0..*" group : belongs to
author "0..*" -- "0..*" book
genre "0..*" -- "0..*" book
@eтьь к