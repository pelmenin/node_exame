const bcrypt = require('bcryptjs');

async function adminAuth(req, res, next) {
    const { authorization } = req.headers;
    if (!authorization || !authorization.startsWith('Basic ')) {
        return res.status(401).json({ message: 'Authentication required.' });
    }

    const base64Credentials = authorization.split(' ')[1];
    const credentials = Buffer.from(base64Credentials, 'base64').toString('ascii');
    const [username, password] = credentials.split(':');

    if (username === "admin" && password === "password") {
        next();
    } else {
        return res.status(401).json({ message: 'Access denied.' });
    }
}

module.exports = adminAuth;
