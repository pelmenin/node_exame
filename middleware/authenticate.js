const bcrypt = require('bcryptjs');
const User = require('../models/user');

async function authenticate(req, res, next) {
  const { authorization } = req.headers;
  if (!authorization || !authorization.startsWith('Basic ')) {
    return res.status(401).send({ message: "Authentication required." });
  }
  const base64Credentials = authorization.split(' ')[1];
  const credentials = Buffer.from(base64Credentials, 'base64').toString('ascii');
  const [username, password] = credentials.split(':');

  try {
    const user = await User.findOne({ where: { username } });
    if (!user) {
      return res.status(401).send({ message: "Invalid credentials." });
    }
    const passwordIsValid = await bcrypt.compare(password, user.password);
    if (!passwordIsValid) {
      return res.status(401).send({ message: "Invalid credentials." });
    }

    req.user = user;
    next();
  } catch (error) {
    res.status(500).send({ message: "Authentication failed", error: error.message });
  }
}

module.exports = authenticate;