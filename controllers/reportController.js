const Report = require('../models/report');
const User = require('../models/user');

exports.createReport = async (req, res) => {
  const { carNumber, description } = req.body;
  try {
    const report = await Report.create({
      carNumber,
      description,
      status: 'new',
      UserId: req.user.id
    });
    res.status(201).json({ message: "Report created successfully", reportId: report.id });
  } catch (error) {
    res.status(500).json({ message: "Error creating report", error: error.message });
  }
};

exports.getUserReports = async (req, res) => {
  try {
      const reports = await Report.findAll({
          where: { UserId: req.user.id }
      });
      res.status(200).json(reports);
  } catch (error) {
      res.status(500).json({ message: "Error retrieving user reports", error: error.message });
  }
};

exports.getReportById = async (req, res) => {
  try {
    const report = await Report.findByPk(req.params.id, {
      include: [{
        model: User,
        attributes: ['fullName']
      }]
    });
    if (report) {
      res.json(report);
    } else {
      res.status(404).send({ message: 'Report not found' });
    }
  } catch (error) {
    res.status(500).json({ message: "Error retrieving report", error: error.message });
  }
};

exports.checkReports = async (req, res) => {
  try {
    const count = await Report.count();
    res.header('Access-Control-Expose-Headers', 'X-Total-Count');
    res.header('X-Total-Count', count).status(204).end();
  } catch (error) {
    res.status(500).json({ message: "Error checking reports", error: error.message });
  }
};

exports.allowMethods = (req, res) => {
  res.header('Access-Control-Allow-Methods', 'GET, POST, HEAD, OPTIONS');
  res.header('Access-Control-Allow-Headers', 'Authorization, Content-Type, X-Total-Count');
  res.header('Access-Control-Expose-Headers', 'X-Total-Count');
  res.status(204).end();
};

exports.getAllReportsAdmin = async (req, res) => {
  try {
    const reports = await Report.findAll({
      include: [{
        model: User,
        attributes: ['fullName'],
      }]
    });
    res.json(reports);
  } catch (error) {
    res.status(500).json({ message: "Error retrieving reports", error: error.message });
  }
};

exports.updateReportAdmin = async (req, res) => {
  const { id } = req.params;
  const { status } = req.body;
  try {
    const report = await Report.findByPk(id);
    if (!report) {
      return res.status(404).json({ message: "Report not found" });
    }
    report.status = status;
    await report.save();
    res.json({ message: "Report status updated successfully", report });
  } catch (error) {
    res.status(500).json({ message: "Error updating report status", error: error.message });
  }
};

exports.deleteReportAdmin = async (req, res) => {
  const { id } = req.params;
  try {
    const report = await Report.findByPk(id);
    if (!report) {
      return res.status(404).json({ message: "Report not found" });
    }
    await report.destroy();
    res.json({ message: "Report deleted successfully" });
  } catch (error) {
    res.status(500).json({ message: "Error deleting report", error: error.message });
  }
};
