const { DataTypes } = require('sequelize');
const sequelize = require('../config/database');
const User = require('./user');

const Report = sequelize.define('Report', {
  carNumber: { type: DataTypes.STRING, allowNull: false },
  description: { type: DataTypes.TEXT, allowNull: false },
  status: { type: DataTypes.STRING, defaultValue: 'new' }
});

Report.belongsTo(User);
User.hasMany(Report);

module.exports = Report;
