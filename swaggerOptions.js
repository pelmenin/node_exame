const swaggerJsdoc = require('swagger-jsdoc');
const swaggerUi = require('swagger-ui-express');
const YAML = require('yamljs'); // npm install yamljs

const options = {
  swaggerDefinition: {
    openapi: '3.0.0',
    info: {
      title: 'Violations.No API',
      version: '1.0.0',
      description: 'This is a REST API for the Violations.No system to report traffic violations.',
      contact: {
        name: 'Developer',
        url: 'http://violations.no',
        email: 'support@violations.no'
      }
    },
    servers: [{
      url: 'http://localhost:3000/api',
      description: 'Development server'
    }],
  },
  apis: ['./routes/*.js'],
};

const swaggerDocument = YAML.load('./api-docs.yaml');

module.exports = (app) => {
  app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));
};
