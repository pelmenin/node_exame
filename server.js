require('dotenv').config();
console.log('JWT Secret:', process.env.JWT_SECRET); 

const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const sequelize = require('./config/database');
const setupSwagger = require('./swaggerOptions');

const userRoutes = require('./routes/users');
const reportRoutes = require('./routes/reports');
const adminRoutes = require('./routes/admin');

const app = express();
const PORT = process.env.PORT || 3000;

app.use(cors());
app.use(bodyParser.json());
app.use(express.static('public'));
app.use(bodyParser.urlencoded({ extended: true }));

setupSwagger(app);

sequelize.sync({ force: false })
    .then(() => {
        console.log('Database tables created/updated successfully.');

        app.use('/api/users', userRoutes);
        app.use('/api/reports', reportRoutes);
        app.use('/api/admin', adminRoutes);

        app.get('/', (req, res) => {
            res.send('Welcome to the Violations.No API');
        });

        app.listen(PORT, () => {
            console.log(`Server running on http://localhost:${PORT}`);
        });
    })
    .catch((error) => {
        console.error('Failed to sync database:', error);
        process.exit(1);
    });

    module.exports = app; // Export the app for testing
