const express = require('express');
const router = express.Router();
const reportController = require('../controllers/reportController');
const authenticate = require('../middleware/authenticate');

router.post('/', authenticate, reportController.createReport);
router.get('/', authenticate, reportController.getUserReports);
router.get('/:id', authenticate, reportController.getReportById);

router.head('/', authenticate, reportController.checkReports);
router.options('/', reportController.allowMethods);

module.exports = router;
