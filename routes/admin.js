const express = require('express');
const router = express.Router();
const reportController = require('../controllers/reportController');
const userController = require('../controllers/userController');
const adminAuth = require('../middleware/adminAuth');

router.post('/login', userController.adminLogin);
router.get('/reports', adminAuth, reportController.getAllReportsAdmin);
router.put('/reports/:id/status', adminAuth, reportController.updateReportAdmin);
router.delete('/reports/:id', adminAuth, reportController.deleteReportAdmin);

module.exports = router;
